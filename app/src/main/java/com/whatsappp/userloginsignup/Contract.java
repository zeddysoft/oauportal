package com.whatsappp.userloginsignup;

import android.provider.BaseColumns;

/**
 * Created by azeez on 12/29/16.
 */
public class Contract {

    private Contract(){

    }

    public static class UserEntry implements BaseColumns{
        public static final String TABLE_NAME = "users";
        public static final String COLUMN_USERNAME = "username";
        public static final String COLUMN_PASSWORD = "password";
        public static final String COLUMN_SESSION = "session";
        public static final String COLUMN_SEMESTER = "semester";
    }

}