package com.whatsappp.userloginsignup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.whatsappp.userloginsignup.error.AuthErrorManager;
import com.whatsappp.userloginsignup.utils.NetworkUtil;
import com.whatsappp.userloginsignup.utils.SnackbarUtils;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView mCreateAccountTV;
    private EditText mEmailET;
    private EditText mPasswordET;
    private Button mSignInBtn;
    private ProgressBar mSignInPB;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private String TAG = "SignInActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

            }
        };

        initViews();
        setClickListeners();
    }

    private void setClickListeners() {
        mCreateAccountTV.setOnClickListener(this);
        mSignInBtn.setOnClickListener(this);
    }


    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }


    private void initViews() {
        mCreateAccountTV = (TextView) findViewById(R.id.create_account_TV);
        mEmailET = (EditText) findViewById(R.id.activity_sign_in_email_ET);
        mPasswordET = (EditText) findViewById(R.id.activity_sign_in_password_ET);
        mSignInBtn = (Button) findViewById(R.id.activity_sign_in_BTN);
        mSignInPB = (ProgressBar) findViewById(R.id.activity_sign_in_PB);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.create_account_TV:
                Intent intent = new Intent(this, SignUpActivity.class);
                startActivity(intent);
                finish();
                break;

            case R.id.activity_sign_in_BTN:
                validateUserInput(mEmailET.getText().toString(),
                        mPasswordET.getText().toString());
                break;
        }
    }


    private void validateUserInput(String email, String password) {
        if (TextUtils.isEmpty(email)) {
            mEmailET.setError("Your sign up email is required");
        } else if (TextUtils.isEmpty(password)) {
            mPasswordET.setError("Your password is required");
        } else {
            doSignIn(email, password);
        }
    }

    private void doSignIn(String email, String password) {

        if (!NetworkUtil.isConnected(App.getAppContext())) {
            SnackbarUtils.show(mSignInBtn,R.string.sign_in_failed);
            return;
        }
        mSignInPB.setVisibility(View.VISIBLE);

        Task<AuthResult> signInTask = mAuth.signInWithEmailAndPassword(email, password);

        signInTask.addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                mSignInPB.setVisibility(View.GONE);

                if (task.isSuccessful()) {
                    launchHomeScreen();
                    finish();
                }else{
                    SnackbarUtils.show(mSignInBtn, getString(R.string.auth_failed));
                }
            }
        });

        signInTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if( e instanceof FirebaseAuthException){
                    String errorCode = ((FirebaseAuthException)e).getErrorCode();
                    AuthErrorManager.displayErrorMessage(errorCode, mSignInBtn);

                }
            }
        });
    }

    private void launchHomeScreen() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }
}