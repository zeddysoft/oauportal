package com.whatsappp.userloginsignup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.whatsappp.userloginsignup.utils.NetworkUtil;
import com.whatsappp.userloginsignup.utils.SnackbarUtils;


public class SignUpActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private String TAG = "SignUpActivity";
    private EditText emailET;
    private EditText passwordET;
    private Button signUpBtn;
    private ProgressBar mProgressBar;
    private TextView mSignInTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sign_up);
        initViews();
        setActionListeners();

        mAuth = FirebaseAuth.getInstance();

    }

    private void setActionListeners() {
        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = emailET.getText().toString();
                String password = passwordET.getText().toString();
                if (isUserCredentialValid(email, password)) {
                    createNewUserAccount(email, password);
                }
            }
        });

        mSignInTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private boolean isUserCredentialValid(String email, String password) {
        if (isEmailValid(email)) {
            if (isPasswordValid(password)) {
                return true;
            } else {
                return false;
            }
        } else {
            SnackbarUtils.show(signUpBtn,"Enter a valid email address");
            return false;
        }
    }

    private boolean isPasswordValid(String password) {
        if (password == null || password.isEmpty()) {
            return false;
        } else if (password.length() < 6) {
            passwordET.setError("Oops, your password must not be less than six characters");
            return false;
        } else {
            return true;
        }
    }

    private boolean isEmailValid(String email) {

        if (email == null)
            return false;
        else if (TextUtils.isEmpty(email)) {
            emailET.setError("Your email is required");
            return false;
        }

        return InputValidator.validateEmail(email);
    }

    private void initViews() {
        emailET = (EditText) findViewById(R.id.activity_sign_up_email_ET);
        passwordET = (EditText) findViewById(R.id.activity_sign_up_password_ET);
        signUpBtn = (Button) findViewById(R.id.activity_sign_up_button_BTN);

        mProgressBar = (ProgressBar) findViewById(R.id.activity_sign_up_PB);
        mSignInTV = (TextView) findViewById(R.id.activity_sign_in_TV);
    }

    private void createNewUserAccount(String email, String password) {

        if (!NetworkUtil.isConnected(App.getAppContext())) {
            SnackbarUtils.show(signUpBtn,R.string.error_network_unavailable);
            return;
        }

        mProgressBar.setVisibility(View.VISIBLE);
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        mProgressBar.setVisibility(View.GONE);

                        if (task.isSuccessful()) {
                            finish();
                            launchHomeScreen();
                        } else {
                            SnackbarUtils.show(signUpBtn, getString(R.string.auth_failed));
                        }
                    }
                });
    }

    private void launchHomeScreen() {
        finish();
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
    }

    @Override
    public void onStart() {
        super.onStart();
//        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
//        if (mAuthListener != null) {
//            mAuth.removeAuthStateListener(mAuthListener);
//        }
    }
}