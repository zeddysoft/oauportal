package com.whatsappp.userloginsignup.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by azeez on 4/4/17.
 */

public class NetworkUtil {
    public static boolean isConnected(Context context) {

        if( context == null){
            Log.d("Context", "null");
        }
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivity.getActiveNetworkInfo();
        // NetworkInfo info
        if (networkInfo != null && networkInfo.isConnected()
                && networkInfo.isAvailable()) {
            return true;
        }
        return false;
    }
}
