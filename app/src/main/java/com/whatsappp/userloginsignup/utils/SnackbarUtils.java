package com.whatsappp.userloginsignup.utils;

import android.support.design.widget.Snackbar;
import android.view.View;

/**
 * Created by azeez on 2/20/17.
 */

public class SnackbarUtils {
    public static void show(View view, String message){
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }

    public static void show(View view, int messageId){
        Snackbar.make(view, messageId, Snackbar.LENGTH_LONG).show();
    }

    public static void show(View view, String message, String actionLabel, View.OnClickListener actionClickListener){
        Snackbar.make(view, message, Snackbar.LENGTH_LONG)
                .setAction(actionLabel, actionClickListener)
                .show();
    }
}
