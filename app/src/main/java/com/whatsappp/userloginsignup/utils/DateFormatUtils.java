package com.whatsappp.userloginsignup.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by azeez on 2/28/17.
 */

public class DateFormatUtils {


    public static String getFormattedDate(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int year = calendar.get(Calendar.YEAR);
        String month = new SimpleDateFormat("MMMM").format(calendar.getTime());

        return day +" " + month + " " + year;
    }

    public static String getTime(Date date){
        return new SimpleDateFormat("HH:mm").format(date);
    }

    public static String getCurrentTimeStamp(){
        return new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
    }

    public static String getRecordDuration(File recordFile){
        return null;
    }
}