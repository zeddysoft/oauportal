package com.whatsappp.userloginsignup.utils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by azeez on 3/1/17.
 */

public class UserUtil {

    public static String getUserId(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        return user.getUid();
    }
}
