package com.whatsappp.userloginsignup;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private TextView usernameET;
    private TextView passwordET;
    private Spinner sessionET;
    private Spinner semesterET;
    private Button loginBT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        createUserAccounts();
        setContentView(R.layout.activity_login);
        initViews();

        loginBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = usernameET.getText().toString();
                String password = passwordET.getText().toString();
                String session = sessionET.getSelectedItem().toString();
                String semester = semesterET.getSelectedItem().toString();

                User user = Db.getUser(username);
                if( user == null){
                    Toast.makeText(LoginActivity.this, "User does not exist", Toast.LENGTH_LONG).show();
                }else{
                    if( !user.getPassword().equals(password)){
                        Toast.makeText(LoginActivity.this, "Password is incorrect", Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(LoginActivity.this, "correct login credentials", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });


    }

    private void initViews() {
        usernameET = (TextView) findViewById(R.id.username);
        passwordET = (TextView) findViewById(R.id.password);
        sessionET = (Spinner) findViewById(R.id.session);
        semesterET = (Spinner) findViewById(R.id.semester);
        loginBT = (Button) findViewById(R.id.login);


        String[] sessions = new String[]{"2014/2015","2015/2016"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, sessions);
        sessionET.setAdapter(adapter);


        String[] semesters = new String[]{"Rain", "Harmattan"};
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, semesters);
        semesterET.setAdapter(adapter2);
    }

    private void createUserAccounts() {
        User user = new User();

        user.setUsername("moses");
        user.setPassword("moses");
        user.setSession("2015/2016");
        user.setSemester("Rain");



        Db.createNewUser(user);
    }
}
