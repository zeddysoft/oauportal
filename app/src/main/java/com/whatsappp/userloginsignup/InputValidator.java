package com.whatsappp.userloginsignup;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by azeez on 2/20/17.
 */

public class InputValidator {

    public static String EMAIL_REGEX_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static boolean validateEmail(String email){
        Pattern pattern = Pattern.compile(EMAIL_REGEX_PATTERN);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }
}
