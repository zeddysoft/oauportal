package com.whatsappp.userloginsignup;

import android.app.Application;
import android.content.Context;

/**
 * Created by blessochampion on 5/1/16.
 */
public class App extends Application {

    private static App sInstance;
    private static Context sContext;


    @Override
    public void onCreate() {

        super.onCreate();
        sInstance = this;
        setAPPContext(getApplicationContext());
    }


    public static Context getAppContext() {
        return sContext;
    }

    private void setAPPContext(Context appContext) {
        sContext = appContext;
    }


    public static App getInstance() {

        return AppHelper.INSTANCE;

    }

    private static class AppHelper {

        private static App INSTANCE = sInstance;

    }
}
