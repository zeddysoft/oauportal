package com.whatsappp.userloginsignup;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by azeez on 12/29/16.
 */
public class DbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;

    static final String DATABASE_NAME = "oauportal.db";

    public static String SQL_CREATE_USER_TABLE = "CREATE TABLE " + Contract.UserEntry.TABLE_NAME + " (" +
            Contract.UserEntry._ID + " INTEGER PRIMARY KEY," +
            Contract.UserEntry.COLUMN_USERNAME + " TEXT," +
            Contract.UserEntry.COLUMN_PASSWORD + " TEXT," +
            Contract.UserEntry.COLUMN_SESSION + " TEXT," +
            Contract.UserEntry.COLUMN_SEMESTER + " TEXT )";


    public DbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_USER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_CREATE_USER_TABLE);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}