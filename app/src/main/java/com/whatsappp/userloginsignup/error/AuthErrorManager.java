package com.whatsappp.userloginsignup.error;

import android.view.View;

import com.whatsappp.userloginsignup.R;
import com.whatsappp.userloginsignup.utils.SnackbarUtils;


/**
 * Created by azeez on 3/17/17.
 */

public class AuthErrorManager {

    public static void displayErrorMessage(String errorCode, View view){
        switch ( errorCode){
            case ErrorCode.WRONG_PASSWORD:
                SnackbarUtils.show(view, R.string.wrong_password);
                break;

            case ErrorCode.USER_NOT_FOUND:
                SnackbarUtils.show(view,"This account does not exist");
                break;
        }
    }
}
