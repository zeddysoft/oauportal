package com.whatsappp.userloginsignup.error;

/**
 * Created by azeez on 3/17/17.
 */

public class ErrorCode {

    public static final String WRONG_PASSWORD = "ERROR_WRONG_PASSWORD";
    public static final String USER_NOT_FOUND = "ERROR_USER_NOT_FOUND";
}
