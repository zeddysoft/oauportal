package com.whatsappp.userloginsignup;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by azeez on 12/30/16.
 */
public class Db {


    static DbHelper dbHelper = new DbHelper(App.getAppContext());

    static SQLiteDatabase writableDb = dbHelper.getWritableDatabase();
    static SQLiteDatabase readableDb = dbHelper.getReadableDatabase();



    public static long createNewUser(User user){

        ContentValues contentValues = new ContentValues();
        contentValues.put(Contract.UserEntry.COLUMN_USERNAME, user.getUsername() );
        contentValues.put(Contract.UserEntry.COLUMN_PASSWORD, user.getPassword() );
        contentValues.put(Contract.UserEntry.COLUMN_SESSION, user.getSession() );
        contentValues.put(Contract.UserEntry.COLUMN_SEMESTER, user.getSemester() );

        return writableDb.insert(Contract.UserEntry.TABLE_NAME, null, contentValues);
    }


    public static User getUser(String username){

        Cursor cursor = readableDb.query(

                Contract.UserEntry.TABLE_NAME,
                null,
                Contract.UserEntry.COLUMN_USERNAME + "=?",
                new String[]{username},
                null,
                null,
                null

        );

        User user = null;
        try {
            boolean isRecordFound = cursor.moveToFirst();

            if( isRecordFound){
                user = new User();
                user.setUsername(cursor.getString(cursor.getColumnIndex
                        (Contract.UserEntry.COLUMN_USERNAME)));
                user.setPassword(cursor.getString(cursor.getColumnIndex
                        (Contract.UserEntry.COLUMN_PASSWORD)));
                user.setSession(cursor.getString(cursor.getColumnIndex
                        (Contract.UserEntry.COLUMN_SESSION)));

                user.setSemester(cursor.getString(cursor.getColumnIndex
                        (Contract.UserEntry.COLUMN_SEMESTER)));
            }


        }finally {
            cursor.close();
        }
        return  user;
    }

}